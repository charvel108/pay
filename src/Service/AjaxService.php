<?php

namespace App\Service;

use \App\Events\AjaxEvent;
use \Exception;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use \Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;


/**
 * Class AjaxService
 * @package App\Service
 */
class AjaxService
{
    protected $container = null;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * Получение контейнера.
     *
     * @return ContainerInterface|null
     */
    protected function getContainer()
    {
        return $this->container;
    }


    /**
     * Обработка ajax-запроса.
     *
     * @param array $data
     * @param Request $request
     * @param JsonResponse $response
     */
    public function process(string $class, string $method, array $data, Request $request, JsonResponse $response)
    {
        if (empty($class) || empty($method)) {
            throw new Exception('Invalid call: method is missing.');
        }

        $event_name = 'ajax.' . $class . '.' . $method;
        $dispatcher = $this->container->get('event_dispatcher');

        if ($dispatcher->hasListeners($event_name)) {
            $event = new AjaxEvent();
            $event->setData($data);
            $event->setRequest($request);
            $event->setResponse($response);

            $dispatcher->dispatch($event_name, $event);

            return $event->getResult();
        }


        // Получение функции обратного вызова.
        $callable = $this->method($class, $method);

        if (!is_callable($callable)) {
            throw new Exception('Invalid call: method cannot be called.');
        }

        list($class, $method) = $callable;

        if (!method_exists($class, $method)) {
            throw new Exception('Invalid call: method is not found.');
        }

        $controller = new $class();
        $controller->setContainer($this->getContainer());

        return call_user_func([$controller, $method], $data, $request, $response);
    }


    /**
     * Получаение метода для вызова.
     *
     * @param $method
     * @return callable|null
     */
    public function method(string $class, string $method)
    {
        if (empty($class) || empty($method)) {
            return null;
        }
        $class  = $this->setSnakeCaseToCamelCase($class, true);
        $method = $this->setSnakeCaseToCamelCase($method);

        $class  = "\\App\\Controller\\Ajax\\{$class}Controller";

        return [$class, $method];
    }


    /**
     * Преборащование snake case в camel case.
     *
     * @param $string
     * @param bool $first
     * @return mixed
     */
    protected function setSnakeCaseToCamelCase($string, $first = false)
    {
        $string = str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $string)));

        if (!$first) {
            $string[0] = strtolower($string[0]);
        }
        return $string;
    }
}