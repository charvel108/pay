<?php

namespace App\Exceptions;

use \Exception;


/**
 * Class AjaxException
 * @package App\Exceptions
 */
class AjaxException extends Exception
{
    protected $data = [];


    /**
     * AjaxException constructor.
     * @param string $message
     * @param null $details
     * @param int $code
     * @param Exception|null $exception
     */
    public function __construct($message = '', $data = [], $code = 0, Exception $exception = null)
    {
        parent::__construct($message, $code, $exception);

        $this->setData($data);
    }


    /**
     * Получение детальной информации.
     *
     * @return |null
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Сохранение детальной информации.
     *
     * @param $details
     * @return $this
     */
    public function setData($data = [])
    {
        $this->data = $data;

        return $this;
    }

}