<?php

namespace App\Utils;


use App\Entity\PaymentData;
use App\Entity\PaymentLink;
use App\Entity\PaymentReceipt;
use App\Entity\PaymentTransaction;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Payment
{
    protected $em = null;
    protected $container = null;

    public function __construct(ContainerInterface $container)
    {
        $this->em = $container->get('doctrine')->getManager();
        $this->container = $container;
    }

    public function isValidData(array $data)
    {
        if (!$data['description']) throw new \Exception('не указан параметр: description');
        if (!$data['amount'] || !ctype_digit($data['amount'])) throw new \Exception('не указан параметр: amount');
        if (!$data['currency'] || !in_array($data['currency'], ['RUB'])) throw new \Exception('не указан параметр: currency');
        if (!$data['type'] || !in_array($data['type'], ['Income'])) throw new \Exception('не указан параметр: type');
        if (!$data['botNumber'] || !ctype_digit($data['botNumber'])) throw new \Exception('не указан параметр: botNumber');

        if (!isset($data['items']) || !is_array($data['items'])) throw new \Exception('не указан или неверный параметр: items');

        foreach ($data['items'] as $item) {
            if (!is_array($item)) throw new \Exception('неверный тип элемента в items');

            if (!isset($item['label'])) throw new \Exception('не указан параметр в одноим из элементов items: label');
            if (!isset($item['amount']) || !ctype_digit($data['amount'])) throw new \Exception('не указан параметр в одноим из элементов items: amount');
            if (!isset($item['quantity']) || !ctype_digit($item['quantity'])) throw new \Exception('не указан параметр в одноим из элементов items: quantity');
            if (!array_key_exists('vat', $item) || !in_array($item['vat'], [null, '20', '10', '0', '110', '120'])) throw new \Exception('не указан параметр в одноим из элементов items: vat');
            if (!isset($item['method']) || !in_array($item['method'], ['0', '1', '2', '3', '4', '5', '6', '7'])) throw new \Exception('не указан параметр в одноим из элементов items: method');
            if (!isset($item['object']) || !in_array($item['object'], ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13'])) throw new \Exception('не указан параметр в одноим из элементов items: object');
            if (!isset($item['measurementUnit']) || !in_array($item['measurementUnit'], ['шт'])) throw new \Exception('не указан параметр в одноим из элементов items: measurementUnit');
        }

        return true;
    }

    public function createLink(array $data): PaymentLink
    {
        $short = substr(md5(time()), 0, 10);
        $accessKey = md5(rand());

        if ($this->em->getRepository(PaymentLink::class)->findBy(['short' => $short]) instanceof PaymentLink) {
            $this->createLink($data);
        }

        $link = new PaymentLink();
        $link->setShort($short);
        $link->setAccessKey($accessKey);

        $this->em->persist($link);
        $this->em->flush();

        $data = $this->createData($data, $link);
        $link->setData($data);

        $this->em->flush();

        return $link;
    }

    public function editLink(PaymentLink $link, array $data): PaymentData
    {
        $paymentData = $link->getData();

        if (!$paymentData instanceof PaymentData) {
            throw new \Exception('данные ссылки не найдены');
        }

        $paymentData->setDescription($data['description']);
        $paymentData->setAmount($data['amount']);
        $paymentData->setCurrency($data['currency']);
        $paymentData->setType($data['type']);
        $paymentData->setItems($data['items']);
        $paymentData->setBotNumber($data['botNumber']);

        $this->em->flush();

        return $paymentData;
    }

    public function deleteLink(PaymentLink $link): bool
    {
        $this->em->remove($link);

        $this->em->flush();

        return true;
    }

    public function createData(array $data, PaymentLink $link): PaymentData
    {
        $paymentData = new PaymentData();
        $paymentData->setDescription($data['description']);
        $paymentData->setAmount($data['amount']);
        $paymentData->setCurrency($data['currency']);
        $paymentData->setType($data['type']);
        $paymentData->setItems($data['items']);
        $paymentData->setBotNumber($data['botNumber']);

        $paymentData->setLink($link);

        $this->em->persist($paymentData);
        $this->em->flush();

        return $paymentData;
    }

    public function createReceipt(array $data, ?PaymentTransaction $transaction): PaymentReceipt
    {
        $receipt = new PaymentReceipt();
        $receipt->setTransaction($transaction);
        $receipt->setReceiptNumber(@$data['Id']);
        $receipt->setDocumentNumber(@$data['DocumentNumber']);
        $receipt->setSessionNumber(@$data['SessionNumber']);
        $receipt->setNumber(@$data['Number']);
        $receipt->setFiscalSign(@$data['FiscalSign']);
        $receipt->setDeviceNumber(@$data['DeviceNumber']);
        $receipt->setRegNumber(@$data['RegNumber']);
        $receipt->setFiscalNumber(@$data['FiscalNumber']);
        $receipt->setInn(@$data['Inn']);
        $receipt->setType(@$data['Type']);
        $receipt->setOfd(@$data['Ofd']);
        $receipt->setUrl(@$data['Url']);
        $receipt->setQrCodeUrl(@$data['QrCodeUrl']);
        $receipt->setTransactionId(@$data['TransactionId']);
        $receipt->setAmount(@$data['Amount']);
        $receipt->setDateTime(new \DateTime(@$data['DateTime']));
        $receipt->setInvoiceId(@$data['InvoiceId']);
        $receipt->setAccountId(@$data['AccountId']);
        $receipt->setReceipt(@$data['Receipt']);
        $receipt->setCalculationPlace(@$data['CalculationPlace']);
        $receipt->setCashierName(@$data['CashierName']);
        $receipt->setSettlePlace(@$data['SettlePlace']);

        $this->em->persist($receipt);
        $this->em->flush();

        return $receipt;
    }

    public function createTransaction(array $data, ?PaymentLink $link): PaymentTransaction
    {
        $transaction = new PaymentTransaction();
        $transaction->setLink($link);
        $transaction->setTransactionId(@$data['TransactionId']);
        $transaction->setAmount(@$data['Amount']);
        $transaction->setCurrency(@$data['Currency']);
        $transaction->setDateTime(new \DateTime(@$data['DateTime']));
        $transaction->setCardFirstSix(@$data['CardFirstSix']);
        $transaction->setCardLastFour(@$data['CardLastFour']);
        $transaction->setCardType(@$data['CardType']);
        $transaction->setCardExpDate(@$data['CardExpDate']);
        $transaction->setTestMode(@$data['TestMode']);
        $transaction->setStatus(@$data['Status']);
        $transaction->setOperationType(@$data['OperationType']);
        $transaction->setGatewayName(@$data['GatewayName']);
        $transaction->setInvoiceId(@$data['InvoiceId']);
        $transaction->setAccountId(@$data['AccountId']);
        $transaction->setSubscriptionId(@$data['SubscriptionId']);
        $transaction->setName(@$data['Name']);
        $transaction->setEmail(@$data['Email']);
        $transaction->setIpAddress(@$data['IpAddress']);
        $transaction->setIpCountry(@$data['IpCountry']);
        $transaction->setIpCity(@$data['IpCity']);
        $transaction->setIpRegion(@$data['IpRegion']);
        $transaction->setIpDistrict(@$data['IpDistrict']);
        $transaction->setIssuer(@$data['Issuer']);
        $transaction->setIssuerBankCountry(@$data['IssuerBankCountry']);
        $transaction->setDescription(@$data['Description']);
        $transaction->setData(@$data['Data']);
        $transaction->setToken(@$data['Token']);

        $this->em->persist($transaction);
        $this->em->flush();

        return $transaction;
    }
}
