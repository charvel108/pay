<?php

namespace App\Utils;


use Symfony\Component\DependencyInjection\ContainerInterface;

class CloudPayments
{
    protected $em = null;
    protected $container = null;

    public function __construct(ContainerInterface $container)
    {
        $this->em = $container->get('doctrine')->getManager();
        $this->container = $container;
    }

    public function requestReceipt(array $data)
    {
        $response = $this->sendRequest('post', '/kkt/receipt/', [
            'Authorization' => 'Basic ' . base64_encode(getenv('CLOUDPAYMENTS_PUBLIC_ID') . ':' . getenv('CLOUDPAYMENTS_API_SECRET')),
        ], [], [
            "Inn" => $data['inn'],              // ИНН
            "Type" => $data['type'],            // признак расчета
            "AccountId" => $data['accountId'],
            'InvoiceId' => $data['invoiceId'],
            "CustomerReceipt" => [
                "Items" => $data['items'],
            ]
        ]);

        return $response;
    }

    private function sendRequest($method, $url, array $headerParams = [], array $urlParams = [], array $bodyParams = [])
    {
        $ch = curl_init();

        $headerParams['Content-Type'] = 'application/json';

        if ($method == 'post') {
            $url = getenv('CLOUDPAYMENTS_API_URL') . $url . '?' . http_build_query($urlParams);

            $data = json_encode($bodyParams);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            $headerParams['Content-Length'] = strlen($data);
        } else {
            $url = getenv('CLOUDPAYMENTS_API_URL') . $url . '?' . http_build_query($urlParams);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array_map(function ($k, $v) {
            return "{$k}: {$v}";
        }, array_keys($headerParams), $headerParams));

        curl_setopt($ch, CURLOPT_HEADER, true);
        $response = curl_exec($ch);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        curl_close($ch);

        $header = substr($response, 0, $headerSize);
        $result = substr($response, $headerSize);

        $data = @json_decode($result, true);

        if (isset($data) && isset($data['Success']) && $data['Success'] !== true) throw new \Exception($data['Message']);

        return $data;
    }
}
