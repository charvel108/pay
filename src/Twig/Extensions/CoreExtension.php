<?php

namespace App\Twig\Extensions;

use Twig\Extension\AbstractExtension;

class CoreExtension extends AbstractExtension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('highlightTextFilter', array($this, 'highlightTextFilter'), array(
                'is_safe' => array('html')
            )),
        );
    }

    /**
     * Подсветка текста при поиске
     *
     * @param $text
     * @param $phrase
     * @param string $highlighter
     * @return null|string|string[]
     */
    public function highlightTextFilter($text, $phrase, $highlighter = '<strong class="highlight">\\1</strong>')
    {
        if (empty($text))
        {
            return '';
        }

        if (empty($phrase))
        {
            return $text;
        }

        if (is_array($phrase) or ($phrase instanceof sfOutputEscaperArrayDecorator))
        {
            foreach ($phrase as $word)
            {
                $pattern[] = '/('.preg_quote($word, '/').')/ui';
                $replacement[] = $highlighter;
            }
        }
        else
        {
            $pattern = '/('.preg_quote($phrase, '/').')/ui';
            $replacement = $highlighter;
        }

        return preg_replace($pattern, $replacement, $text);
    }
}