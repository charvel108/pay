<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentReceiptRepository")
 * @ORM\Table(name="payment_receipts")
 */
class PaymentReceipt
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $receiptNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $documentNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $sessionNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $fiscalSign;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $deviceNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $regNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $fiscalNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ofd;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $qrCodeUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $transactionId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateTime;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $invoiceId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $accountId;

    /**
     * @var array
     *
     * @ORM\Column(type="array", nullable=true)
     */
    private $receipt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $calculationPlace;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cashierName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $settlePlace;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PaymentTransaction")
     * @ORM\JoinColumn(name="transaction", referencedColumnName="id")
     */
    protected $transaction;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get receiptNumber.
     *
     * @return string|null
     */
    public function getReceiptNumber(): ?string
    {
        return $this->receiptNumber;
    }

    /**
     * Set receiptNumber.
     *
     * @param string|null $receiptNumber
     *
     * @return void
     */
    public function setReceiptNumber(?string $receiptNumber): void
    {
        $this->receiptNumber = $receiptNumber;
    }

    /**
     * Get documentNumber.
     *
     * @return string|null
     */
    public function getDocumentNumber(): ?string
    {
        return $this->documentNumber;
    }

    /**
     * Set documentNumber.
     *
     * @param string|null $documentNumber
     *
     * @return void
     */
    public function setDocumentNumber(?string $documentNumber): void
    {
        $this->documentNumber = $documentNumber;
    }

    /**
     * Get sessionNumber.
     *
     * @return string|null
     */
    public function getSessionNumber(): ?string
    {
        return $this->sessionNumber;
    }

    /**
     * Set sessionNumber.
     *
     * @param string|null $sessionNumber
     *
     * @return void
     */
    public function setSessionNumber(?string $sessionNumber): void
    {
        $this->sessionNumber = $sessionNumber;
    }

    /**
     * Get number.
     *
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * Set number.
     *
     * @param string|null $number
     *
     * @return void
     */
    public function setNumber(?string $number): void
    {
        $this->number = $number;
    }

    /**
     * Get fiscalSign.
     *
     * @return string|null
     */
    public function getFiscalSign(): ?string
    {
        return $this->fiscalSign;
    }

    /**
     * Set fiscalSign.
     *
     * @param string|null $fiscalSign
     *
     * @return void
     */
    public function setFiscalSign(?string $fiscalSign): void
    {
        $this->fiscalSign = $fiscalSign;
    }

    /**
     * Get deviceNumber.
     *
     * @return string|null
     */
    public function getDeviceNumber(): ?string
    {
        return $this->deviceNumber;
    }

    /**
     * Set deviceNumber.
     *
     * @param string|null $deviceNumber
     *
     * @return void
     */
    public function setDeviceNumber(?string $deviceNumber): void
    {
        $this->deviceNumber = $deviceNumber;
    }

    /**
     * Get regNumber.
     *
     * @return string|null
     */
    public function getRegNumber(): ?string
    {
        return $this->regNumber;
    }

    /**
     * Set regNumber.
     *
     * @param string|null $regNumber
     *
     * @return void
     */
    public function setRegNumber(?string $regNumber): void
    {
        $this->regNumber = $regNumber;
    }

    /**
     * Get fiscalNumber.
     *
     * @return string|null
     */
    public function getFiscalNumber(): ?string
    {
        return $this->fiscalNumber;
    }

    /**
     * Set fiscalNumber.
     *
     * @param string|null $fiscalNumber
     *
     * @return void
     */
    public function setFiscalNumber(?string $fiscalNumber): void
    {
        $this->fiscalNumber = $fiscalNumber;
    }

    /**
     * Get inn.
     *
     * @return string|null
     */
    public function getInn(): ?string
    {
        return $this->inn;
    }

    /**
     * Set inn.
     *
     * @param string|null $inn
     *
     * @return void
     */
    public function setInn(?string $inn): void
    {
        $this->inn = $inn;
    }

    /**
     * Get type.
     *
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return void
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * Get ofd.
     *
     * @return string|null
     */
    public function getOfd(): ?string
    {
        return $this->ofd;
    }

    /**
     * Set ofd.
     *
     * @param string|null $ofd
     *
     * @return void
     */
    public function setOfd(?string $ofd): void
    {
        $this->ofd = $ofd;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return void
     */
    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    /**
     * Get qrCodeUrl.
     *
     * @return string|null
     */
    public function getQrCodeUrl(): ?string
    {
        return $this->qrCodeUrl;
    }

    /**
     * Set qrCodeUrl.
     *
     * @param string|null $qrCodeUrl
     *
     * @return void
     */
    public function setQrCodeUrl(?string $qrCodeUrl): void
    {
        $this->qrCodeUrl = $qrCodeUrl;
    }

    /**
     * Get transactionId.
     *
     * @return string|null
     */
    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    /**
     * Set transactionId.
     *
     * @param string|null $transactionId
     *
     * @return void
     */
    public function setTransactionId(?string $transactionId): void
    {
        $this->transactionId = $transactionId;
    }

    /**
     * Get amount.
     *
     * @return string|null
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * Set amount.
     *
     * @param string $amount
     *
     * @return void
     */
    public function setAmount(?string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * Get dateTime.
     *
     * @return \DateTime|null
     */
    public function getDateTime(): ?\DateTime
    {
        return $this->dateTime;
    }

    /**
     * Set dateTime.
     *
     * @param \DateTime|null $dateTime
     *
     * @return void
     */
    public function setDateTime(?\DateTime $dateTime): void
    {
        $this->dateTime = $dateTime;
    }

    /**
     * Get invoiceId.
     *
     * @return string|null
     */
    public function getInvoiceId(): ?string
    {
        return $this->invoiceId;
    }

    /**
     * Set invoiceId.
     *
     * @param string|null $invoiceId
     *
     * @return void
     */
    public function setInvoiceId(?string $invoiceId): void
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * Get accountId.
     *
     * @return string|null
     */
    public function getAccountId(): ?string
    {
        return $this->accountId;
    }

    /**
     * Set accountId.
     *
     * @param string|null $accountId
     *
     * @return void
     */
    public function setAccountId(?string $accountId): void
    {
        $this->accountId = $accountId;
    }

    /**
     * Get receipt.
     *
     * @return array|null
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * Set receipt.
     *
     * @param array|null $receipt
     *
     * @return void
     */
    public function setReceipt($receipt): void
    {
        $this->receipt = $receipt;
    }

    /**
     * Get calculationPlace.
     *
     * @return string|null
     */
    public function getCalculationPlace(): ?string
    {
        return $this->calculationPlace;
    }

    /**
     * Set calculationPlace.
     *
     * @param string|null $calculationPlace
     *
     * @return void
     */
    public function setCalculationPlace(?string $calculationPlace): void
    {
        $this->calculationPlace = $calculationPlace;
    }

    /**
     * Get cashierName.
     *
     * @return string|null
     */
    public function getCashierName(): ?string
    {
        return $this->cashierName;
    }

    /**
     * Set cashierName.
     *
     * @param string|null $cashierName
     *
     * @return void
     */
    public function setCashierName(?string $cashierName): void
    {
        $this->cashierName = $cashierName;
    }

    /**
     * Get settlePlace.
     *
     * @return string|null
     */
    public function getSettlePlace(): ?string
    {
        return $this->settlePlace;
    }

    /**
     * Set settlePlace.
     *
     * @param string|null $settlePlace
     *
     * @return void
     */
    public function setSettlePlace(?string $settlePlace): void
    {
        $this->settlePlace = $settlePlace;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime|null $createdAt
     *
     * @return void
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set transaction.
     *
     * @param PaymentTransaction|null $transaction
     *
     * @return void
     */
    public function setTransaction(?PaymentTransaction $transaction = null): void
    {
        $this->transaction = $transaction;
    }

    /**
     * Get transaction.
     *
     * @return PaymentTransaction|null
     */
    public function getTransaction(): ?PaymentTransaction
    {
        return $this->transaction;
    }
}
