<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentLinkRepository")
 * @ORM\Table(name="payment_links")
 */
class PaymentLink
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $short;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $accessKey;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PaymentData")
     * @ORM\JoinColumn(name="data", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $data;

    
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get short.
     *
     * @return string
     */
    public function getShort()
    {
        return $this->short;
    }


    /**
     * Set short.
     *
     * @param string $short
     *
     * @return void
     */
    public function setShort($short)
    {
        $this->short = $short;
    }

    /**
     * Get accessKey.
     *
     * @return string
     */
    public function getAccessKey()
    {
        return $this->accessKey;
    }


    /**
     * Set accessKey.
     *
     * @param string $accessKey
     *
     * @return void
     */
    public function setAccessKey($accessKey)
    {
        $this->accessKey = $accessKey;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return void
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set data.
     *
     * @param PaymentData|null $data
     *
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Get data.
     *
     * @return PaymentData|null
     */
    public function getData()
    {
        return $this->data;
    }
}
