<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentTransactionRepository")
 * @ORM\Table(name="payment_transactions")
 */
class PaymentTransaction
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $transactionId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $currency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateTime;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardFirstSix;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardLastFour;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardExpDate;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $testMode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $operationType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $gatewayName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $invoiceId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $subscriptionId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ipAddress;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ipCountry;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ipCity;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ipRegion;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ipDistrict;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $issuer;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $issuerBankCountry;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @var array
     *
     * @ORM\Column(type="array", nullable=true)
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $token;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PaymentLink")
     * @ORM\JoinColumn(name="link", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $link;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get transactionId.
     *
     * @return string|null
     */
    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    /**
     * Set transactionId.
     *
     * @param string $transactionId
     *
     * @return void
     */
    public function setTransactionId(?string $transactionId): void
    {
        $this->transactionId = $transactionId;
    }

    /**
     * Get amount.
     *
     * @return string|null
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * Set amount.
     *
     * @param string $amount
     *
     * @return void
     */
    public function setAmount(?string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * Get currency.
     *
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * Set currency.
     *
     * @param string $currency
     *
     * @return void
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * Get dateTime.
     *
     * @return \DateTime|null
     */
    public function getDateTime(): ?\DateTime
    {
        return $this->dateTime;
    }

    /**
     * Set dateTime.
     *
     * @param \DateTime $dateTime
     *
     * @return void
     */
    public function setDateTime(?\DateTime $dateTime): void
    {
        $this->dateTime = $dateTime;
    }

    /**
     * Get cardFirstSix.
     *
     * @return string|null
     */
    public function getCardFirstSix(): ?string
    {
        return $this->cardFirstSix;
    }

    /**
     * Set cardFirstSix.
     *
     * @param string $cardFirstSix
     *
     * @return void
     */
    public function setCardFirstSix(?string $cardFirstSix): void
    {
        $this->cardFirstSix = $cardFirstSix;
    }

    /**
     * Get cardLastFour.
     *
     * @return string|null
     */
    public function getCardLastFour(): ?string
    {
        return $this->cardLastFour;
    }

    /**
     * Set cardLastFour.
     *
     * @param string $cardLastFour
     *
     * @return void
     */
    public function setCardLastFour(?string $cardLastFour): void
    {
        $this->cardLastFour = $cardLastFour;
    }

    /**
     * Get cardType.
     *
     * @return string|null
     */
    public function getCardType(): ?string
    {
        return $this->cardType;
    }

    /**
     * Set cardType.
     *
     * @param string $cardType
     *
     * @return void
     */
    public function setCardType(?string $cardType): void
    {
        $this->cardType = $cardType;
    }

    /**
     * Get cardExpDate.
     *
     * @return string|null
     */
    public function getCardExpDate(): ?string
    {
        return $this->cardExpDate;
    }

    /**
     * Set cardExpDate.
     *
     * @param string $cardExpDate
     *
     * @return void
     */
    public function setCardExpDate(?string $cardExpDate): void
    {
        $this->cardExpDate = $cardExpDate;
    }

    /**
     * Get testMode.
     *
     * @return bool|null
     */
    public function getTestMode(): ?bool
    {
        return $this->testMode;
    }

    /**
     * Set testMode.
     *
     * @param bool $testMode
     *
     * @return void
     */
    public function setTestMode(?bool $testMode): void
    {
        $this->testMode = $testMode;
    }

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return void
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * Get operationType.
     *
     * @return string|null
     */
    public function getOperationType(): ?string
    {
        return $this->operationType;
    }

    /**
     * Set operationType.
     *
     * @param string $operationType
     *
     * @return void
     */
    public function setOperationType(?string $operationType): void
    {
        $this->operationType = $operationType;
    }

    /**
     * Get gatewayName.
     *
     * @return string|null
     */
    public function getGatewayName(): ?string
    {
        return $this->gatewayName;
    }

    /**
     * Set gatewayName.
     *
     * @param string $gatewayName
     *
     * @return void
     */
    public function setGatewayName(?string $gatewayName): void
    {
        $this->gatewayName = $gatewayName;
    }

    /**
     * Get invoiceId.
     *
     * @return string|null
     */
    public function getInvoiceId(): ?string
    {
        return $this->invoiceId;
    }

    /**
     * Set invoiceId.
     *
     * @param string $invoiceId
     *
     * @return void
     */
    public function setInvoiceId(?string $invoiceId): void
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * Get accountId.
     *
     * @return string|null
     */
    public function getAccountId(): ?string
    {
        return $this->accountId;
    }

    /**
     * Set accountId.
     *
     * @param string $accountId
     *
     * @return void
     */
    public function setAccountId(?string $accountId): void
    {
        $this->accountId = $accountId;
    }

    /**
     * Get subscriptionId.
     *
     * @return string|null
     */
    public function getSubscriptionId(): ?string
    {
        return $this->subscriptionId;
    }

    /**
     * Set subscriptionId.
     *
     * @param string $subscriptionId
     *
     * @return void
     */
    public function setSubscriptionId(?string $subscriptionId): void
    {
        $this->subscriptionId = $subscriptionId;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return void
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * Get email;
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set email;.
     *
     * @param string $email
     *
     * @return void
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * Get ipAddress;
     *
     * @return string|null
     */
    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    /**
     * Set ipAddress;
     *
     * @param string $ipAddress
     *
     * @return void
     */
    public function setIpAddress(?string $ipAddress): void
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * Get ipCountry;
     *
     * @return string|null
     */
    public function getIpCountry(): ?string
    {
        return $this->ipCountry;
    }

    /**
     * Set ipCountry;
     *
     * @param string $ipCountry
     *
     * @return void
     */
    public function setIpCountry(?string $ipCountry): void
    {
        $this->ipCountry = $ipCountry;
    }

    /**
     * Get ipCity;
     *
     * @return string|null
     */
    public function getIpCity(): ?string
    {
        return $this->ipCity;
    }

    /**
     * Set ipCity;
     *
     * @param string $ipCity
     *
     * @return void
     */
    public function setIpCity(?string $ipCity): void
    {
        $this->ipCity = $ipCity;
    }

    /**
     * Get ipRegion;
     *
     * @return string|null
     */
    public function getIpRegion(): ?string
    {
        return $this->ipRegion;
    }

    /**
     * Set ipRegion;
     *
     * @param string $ipRegion
     *
     * @return void
     */
    public function setIpRegion(?string $ipRegion): void
    {
        $this->ipRegion = $ipRegion;
    }

    /**
     * Get ipDistrict;
     *
     * @return string|null
     */
    public function getIpDistrict(): ?string
    {
        return $this->ipDistrict;
    }

    /**
     * Set ipDistrict;
     *
     * @param string $ipDistrict
     *
     * @return void
     */
    public function setIpDistrict(?string $ipDistrict): void
    {
        $this->ipDistrict = $ipDistrict;
    }

    /**
     * Get issuer;
     *
     * @return string|null
     */
    public function getIssuer(): ?string
    {
        return $this->issuer;
    }

    /**
     * Set issuer;
     *
     * @param string $issuer
     *
     * @return void
     */
    public function setIssuer(?string $issuer): void
    {
        $this->issuer = $issuer;
    }

    /**
     * Get issuerBankCountry;
     *
     * @return string|null
     */
    public function getIssuerBankCountry(): ?string
    {
        return $this->issuerBankCountry;
    }

    /**
     * Set issuerBankCountry;
     *
     * @param string $issuerBankCountry
     *
     * @return void
     */
    public function setIssuerBankCountry(?string $issuerBankCountry): void
    {
        $this->issuerBankCountry = $issuerBankCountry;
    }

    /**
     * Get description;
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description;
     *
     * @param string $description
     *
     * @return void
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * Get data.
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set data.
     *
     * @param array $data
     *
     * @return void
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * Get token;
     *
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * Set token;
     *
     * @param string $token
     *
     * @return void
     */
    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return void
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set link.
     *
     * @param PaymentLink|null $link
     *
     * @return void
     */
    public function setLink(?PaymentLink $link): void
    {
        $this->link = $link;
    }

    /**
     * Get link.
     *
     * @return PaymentLink|null
     */
    public function getLink(): ?PaymentLink
    {
        return $this->link;
    }
}
