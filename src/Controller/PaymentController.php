<?php

namespace App\Controller;

use App\Entity\PaymentData;
use App\Entity\PaymentLink;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class PaymentController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $short
     * @param string $accountId
     * @return Response
     */
    public function pay(Request $request, string $short, ?string $accountId = null): Response
    {
        $em = $this->getDoctrine()->getManager();

        $link = $em->getRepository(PaymentLink::class)->findOneBy(['short' => $short]);
        if (!$link instanceof PaymentLink) {
            throw $this->createNotFoundException('ссылка не найдена');
        }

        $data = $link->getData();
        if (!$data instanceof PaymentData) {
            throw $this->createNotFoundException('данные не найдены');
        }

        return $this->render('payment/index.html.twig', [
            'publicId' => getenv('CLOUDPAYMENTS_PUBLIC_ID'),
            'description' => $data->getDescription(),
            'amount' => $data->getAmount(),
            'currency' => $data->getCurrency(),
            'accountId' => $accountId,

            'short' => $short,
            'botNumber' => $data->getBotNumber(),
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function success(Request $request): Response
    {
        return $this->render('payment/success.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function fail(Request $request): Response
    {
        return $this->render('payment/fail.html.twig');
    }
}
