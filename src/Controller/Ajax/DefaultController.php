<?php

namespace App\Controller\Ajax;

use \App\Entity\Bot;
use \App\Entity\Channel;
use \App\Entity\Notification;
use \App\Exceptions\AjaxException;
use \App\Security\UsernamePasswordAuthenticatorForm;
use \App\Service\BotService;
use \App\Service\UserService;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\PaymentLink;
use App\Entity\PaymentData;

/**
 * Class DefaultController
 * @package App\Controller\Ajax
 */
class DefaultController extends AbstractController
{

    /**
     * Создание новой ссылки для оплаты или редактирование старой
     *
     * @param array $data
     * @return array
     * @throws AjaxException
     */
    public function getLinkData(array $data = [])
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        // id ссылки
        $id = @$data['id'];

        $link = new PaymentLink();
        $linkData = new PaymentData();

        $quantity = null;
        $vat = 'null';
        $method = 0;
        $object = 0;
        $measurementUnit = 'шт';

        // Если получили id, то редактируем ссылку
        if ($id) {
            $link = $em->getRepository(PaymentLink::class)->findOneBy(['id' => $id]);
            $linkData = $em->getRepository(PaymentData::class)->findOneBy(['link' => $id]);

            if (!$linkData instanceof PaymentData || !$link instanceof PaymentLink) $errors['global'] = 'Ссылка с таким id не найдена';

            $items = json_decode($linkData->getItems());
            if (!$items) $errors['global'] = 'Не удалось получить данные';

            $quantity = $items->quantity;
            $vat = $items->vat;
            $method = $items->method;
            $object = $items->object;
            $measurementUnit = $items->measurementUnit;
        }

        if (count($errors)) throw new AjaxException('invalid call', $errors);

        return [
            'id'        => $id,
            'amount'    => $linkData->getAmount(),
            'desc'      => $linkData->getDescription(),
            'currency'  => $linkData->getCurrency(),
            'bot'       => $linkData->getBotNumber(),
            'link'      => $link->getShort(),
            'quantity'  => $quantity,
            'vat'       => $vat,
            'method'    => $method,
            'object'    => $object,
            'measurementUnit' => $measurementUnit,
        ];
    }

    /**
     * Сохранение ссылки в базу
     *
     * @param array $data
     * @return array
     * @throws AjaxException
     */
    public function setLinkData(array $data = []) {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $form       = (array) @$data['form'];
        $id         = @$form['id'];
        $amount     = trim(@$form['amount']);
        $desc       = trim(@$form['desc']);
        $currency   = trim(@$form['currency']);
        $bot        = trim(@$form['bot']);
        $link       = substr(md5(time()), 0, 10);

        $paymentLink = new PaymentLink();
        $paymentData = new PaymentData();

        if ($id) {
            $paymentLink = $this->getDoctrine()->getRepository(PaymentLink::class)->findOneBy(['id'=>$id]);
            $paymentData = $this->getDoctrine()->getRepository(PaymentData::class)->findOneBy(['link'=>$id]);

            if (!$paymentLink instanceof PaymentLink || !$paymentData instanceof PaymentData) $errors['global'] = 'Ссылка с таким id не найдена';
        }

        if (!$amount) {
            $errors['amount'] = 'Введите сумму';
        }
        if (!$desc) {
            $errors['desc'] = 'Введите описание';
        }
        if (!$currency) {
            $errors['currency'] = 'Укажите валюту';
        }
        if (!$bot) {
            $errors['bot'] = 'Введите номер бота';
        }

        $quantity = trim(@$form['quantity']);
        if (!$quantity || !ctype_digit($quantity)) $errors['quantity'] = 'Неверно заполнено количество';

        $vat = trim(@$form['vat']);
        if (!array_key_exists('vat', $form) || !in_array($vat, ['null', '20', '10', '0', '110', '120'])) $errors['vat'] = 'Неверно заполнено налогооблажение: ' . $vat;

        $object = trim(@$form['object']);
        if (!isset($object) || !in_array($object, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13'])) $errors['object'] = 'Неверно заполнен предмет оплаты';

        $measurementUnit = trim(@$form['measurementUnit']);
        if (!$measurementUnit || !in_array($measurementUnit, ['шт'])) $errors['measurementUnit'] = 'Неверно заполнен признак предмета';

        $method = trim(@$form['method']);
        if (!isset($object) || !in_array($method, ['0', '1', '2', '3', '4', '5', '6', '7'])) $errors['method'] = 'Неверно заполнен способ расчета';

        if (count($errors)) throw new AjaxException('invalid call', $errors);

        $paymentLink->setShort($link);
        $em->persist($paymentLink);
        $em->flush();

        $items = [
            'label'             => $desc,
            'amount'            => $amount,
            'quantity'          => $quantity,
            'vat'               => $vat,
            'method'            => $method,
            'object'            => $object,
            'measurementUnit'   => $measurementUnit,
        ];
        $items = json_encode($items);

        $paymentData->setDescription($desc);
        $paymentData->setAmount($amount);
        $paymentData->setCurrency($currency);
        $paymentData->setType("Admin");
        $paymentData->setItems($items);
        $paymentData->setBotNumber($bot);

        $paymentData->setLink($paymentLink);

        $em->persist($paymentData);
        $em->flush();

        $paymentLink->setData($paymentData);
        $em->flush();

        return [ 'result' => true ];
    }

    /**
     * Удаление ссылки
     *
     * @param array $data
     * @return array
     * @throws AjaxException
     */
    public function deleteLink(array $data = []) {

        $em = $this->getDoctrine()->getManager();
        $id = @$data['id'];
        $link = $this->getDoctrine()->getRepository(PaymentLink::class)->findOneBy(['id'=>$id]);

        $em->remove($link);
        $em->flush();

        return $id;
    }
}
