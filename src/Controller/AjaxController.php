<?php

namespace App\Controller;

use \App\Service\AjaxService;
use \App\Exceptions\AjaxException;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use \Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;


/**
 * Класс, отвечающий за маршрутизацию ajax-запросов.
 *
 * @package App\Controller
 */
class AjaxController extends AbstractController
{

    /**
     * Обработка ajax-запросов.
     *
     * @param Request $request
     */
    public function gateway(string $class, string $method, Request $request, AjaxService $service)
    {
        // Данные запроса.
        $data = (array) $request->request->all();

        // Ответ.
        $response = new JsonResponse();


        $failed = false;
        try {
            $result = $service->process($class, $method, $data, $request, $response);
        } catch (AjaxException $e) {
            $failed = true;
            $error  = ['status' => false, 'message' => $e->getMessage(), 'data' => $e->getData()];
        } catch (Exception $e) {
            $failed = true;
            $error  = ['status' => false, 'message' => $e->getMessage()];
        }


        // Установка данных ответа.
        if (!$failed) {
            $response->setData(['status' => true, 'data' => $result]);
        } else {
            $response->setData($error);
        }

        return $response;
    }
}
