<?php

namespace App\Controller;

use App\Entity\PaymentData;
use App\Entity\PaymentLink;
use App\Entity\PaymentReceipt;
use App\Entity\PaymentTransaction;
use App\Utils\Payment;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use App\Utils\CloudPayments;


class PaymentApiController extends AbstractController
{
    /**
     * @param Request $request
     * @param Payment $payment
     * @return JsonResponse
     */
    public function createLink(Request $request, Payment $payment)
    {
        $content = $request->getContent();

        $data = json_decode($content, true);

        try {
            if (!is_array($data) || !$payment->isValidData($data)) {
                throw new \Exception('невалидные данные');
            }

            $link = $payment->createLink($data);
            if (!$link instanceof PaymentLink) {
                throw new \Exception('ошибка создания ссылки');
            }
        } catch (\Exception $e) {
            return new JsonResponse(['status' => false, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['status' => true, 'message' => 'ссылка создана', 'short' => $link->getShort(), 'accessKey' => $link->getAccessKey()]);
    }

    /**
     * @param Request $request
     * @param Payment $payment
     * @param string $short
     * @return JsonResponse
     */
    public function editLink(Request $request, Payment $payment, string $short)
    {
        $em = $this->getDoctrine()->getManager();

        $content = $request->getContent();

        $data = json_decode($content, true);

        try {
            if (!array_key_exists('accessKey', $data)) throw new \Exception('не указан параметр: accessKey');

            if (!is_array($data) || !$payment->isValidData($data)) {
                throw new \Exception('невалидные данные');
            }

            $link = $em->getRepository(PaymentLink::class)->findOneBy(['short' => $short]);
            if (!$link instanceof PaymentLink) {
                throw $this->createNotFoundException('ссылка не найдена');
            }

            if ($link->getAccessKey() != $data['accessKey']) {
                throw new \Exception('неправомерный доступ к ссылке');
            }

            $link = $payment->editLink($link, $data);
            if (!$link) {
                throw new \Exception('ошибка изменения ссылки');
            }
        } catch (\Exception $e) {
            return new JsonResponse(['status' => false, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['status' => true, 'message' => 'ссылка изменена']);
    }

    /**
     * @param Request $request
     * @param Payment $payment
     * @param string $short
     * @return JsonResponse
     */
    public function deleteLink(Request $request, Payment $payment, string $short)
    {
        $em = $this->getDoctrine()->getManager();

        $content = $request->getContent();

        $data = json_decode($content, true);

        try {
            if (!array_key_exists('accessKey', $data)) throw new \Exception('не указан параметр: accessKey');

            $link = $em->getRepository(PaymentLink::class)->findOneBy(['short' => $short]);
            if (!$link instanceof PaymentLink) {
                throw $this->createNotFoundException('ссылка не найдена');
            }

            if ($link->getAccessKey() != $data['accessKey']) {
                throw new \Exception('неправомерный доступ к ссылке');
            }

            $link = $payment->deleteLink($link);
            if (!$link) {
                throw new \Exception('ошибка удаления ссылки');
            }
        } catch (\Exception $e) {
            return new JsonResponse(['status' => false, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['status' => true, 'message' => 'ссылка удалена']);
    }

    /**
     * @param Request $request
     * @param CloudPayments $cloudPayments
     * @param Payment $payment
     * @param LoggerInterface $logger
     * @param string $slug
     * @return JsonResponse
     */
    public function pay(Request $request, LoggerInterface $logger, CloudPayments $cloudPayments, Payment $payment, string $slug)
    {
        $em = $this->get('doctrine')->getManager();

        try {
            if ($slug !== getenv('CLOUDPAYMENTS_API_SLUG')) {
                throw new \Exception("wrong slug");
            }

            $apiSecret = getenv('CLOUDPAYMENTS_API_SECRET');

            $headers = $request->headers->all();
            $fields = $request->request->all();

            $data = json_decode($fields['Data'], true);

            $hmac_come = reset($headers['content-hmac']);
            $hmac_calc = base64_encode(hash_hmac('sha256', $request->getContent(), $apiSecret, true));

            if ($hmac_come !== $hmac_calc) {
                throw new \Exception("wrong request");
            }

            if ($em->getRepository(PaymentTransaction::class)->findBy(['transactionId' => @$data['TransactionId']]) instanceof PaymentTransaction) {
                return new JsonResponse(['code' => 0]);
            }

            $link = $em->getRepository(PaymentLink::class)->findOneBy(['short' => @$data['short']]);

            $payment->createTransaction($fields, $link);

            if ($link instanceof PaymentLink) {
                $paymentData = $link->getData();
                if ($paymentData instanceof PaymentData) {
                    $cloudPayments->requestReceipt([
                        'inn' => $this->getParameter('izzibot.inn'),
                        'accountId' => @$fields['AccountId'],
                        'invoiceId' => @$fields['TransactionId'],
                        'type' => $paymentData->getType(),
                        'items' => $paymentData->getItems()
                    ]);
                }
            }

        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            return new JsonResponse(['code' => -1, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['code' => 0]);
    }

    /**
     * @param Request $request
     * @param string $slug
     * @param LoggerInterface $logger
     * @param Payment $payment
     * @return JsonResponse
     */
    public function receipt(Request $request, LoggerInterface $logger, Payment $payment, string $slug)
    {
        $em = $this->get('doctrine')->getManager();

        try {
            if ($slug !== getenv('CLOUDPAYMENTS_API_SLUG')) {
                throw new \Exception("wrong slug");
            }

            $apiSecret = getenv('CLOUDPAYMENTS_API_SECRET');

            $headers = $request->headers->all();
            $fields = $request->request->all();

            $hmac_come = reset($headers['content-hmac']);
            $hmac_calc = base64_encode(hash_hmac('sha256', $request->getContent(), $apiSecret, true));

            if ($hmac_come !== $hmac_calc) {
                throw new \Exception("wrong request");
            }

            $transaction = $em->getRepository(PaymentTransaction::class)->findOneBy(['transactionId' => @$fields['InvoiceId']]);

            if ($em->getRepository(PaymentReceipt::class)->findBy(['transaction' => $transaction]) instanceof PaymentReceipt) {
                return new JsonResponse(['code' => 0]);
            }

            $payment->createReceipt($fields, $transaction);

        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            return new JsonResponse(['code' => -1, 'message' => $e->getMessage()]);
        }

        return new JsonResponse(['code' => 0]);
    }
}
