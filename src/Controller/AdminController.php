<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Entity\PaymentData;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Knp\Bundle\PaginatorBundle\Templating\PaginationHelper;

class AdminController extends Controller
{
    /**
     * @Template ()
     */
    public function paymentsAction(Request $req) {
        $filter = (array) $req->get('filter');
        $filter['query'] = trim(@$filter['query']);

        $p = [];
        $p['page'] = $req->get('page', 1);

        if ($filter['query']) {
            $p['query'] = $filter['query'];
        }

        return [
            'payments'  => $this->getPaymentsWithPagingByParameters($p),
            'filter'    => $filter,
        ];
    }

    /**
     * Пагинация
     */
    public function getPaymentsWithPagingByParameters(array $p)
    {
        $em = $this->get('doctrine')->getManager();
        $limitPerPage = $this->getParameter('limitPerPage', 10);

        $page = (int) @$p['page'] ? @$p['page'] : 1;

        $linkDataEntity = $em->getRepository(PaymentData::class);
        $qb = $linkDataEntity->createQueryBuilder('d');
        $qb->select('d, l, IDENTITY(d.link) as dataLink');
        $qb->leftJoin('d.link', 'l');
        $qb->where('d.type = :type');
        $qb->setParameter('type', 'Admin');

        if (isset($p['query']) && $p['query']) {
            $qb->andWhere('l.short LIKE :query OR d.description LIKE :query OR d.amount LIKE :query OR d.botNumber LIKE :query');
            $qb->setParameter('query', '%' . $p['query'] . '%');
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb->getQuery(),
            $page,
            $limitPerPage, [
                'defaultSortFieldName' => 'd.createdAt',
                'defaultSortDirection' => 'DESC'
            ]
        );

        return $pagination;
    }
}