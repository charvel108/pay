
var appasyncadd = appasyncadd || [];

(function() {

    // Приложение.
    var app = {

        // Получение домена.
        getBaseDomain: function() {
            let parts;

            parts = location.hostname.split('.');
            parts = parts.slice(-2).join('.');

            return parts;
        },

        // Общие функции.
        misc: {
            unique: function(elems) {
                var uniques = [];
                for (let i = 0; i < elems.length; i++) {
                    var found = false;
                    for (var j = 0; j < uniques.length; j++) {
                        if (elems[i] === uniques[j]) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        uniques.push(elems[i]);
                    }
                }
                return uniques;
            },

            br2nl: function(str) {
                if (typeof str !== 'string') return str;
                return str.replace(/<br\s*\/?>/mg,"\n");
            },

            nl2br: function(str, is_xhtml) {
                if (typeof str === 'undefined' || str === null) {
                    return '';
                }
                var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
                return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
            },

            loader: {
                show: function() {
                    $('.jsLoader').show();
                },
                hide: function() {
                    $('.jsLoader').hide();
                }
            },

            // Модальное окно-уведмоление.
            modal: {
                show: function(text, head = '', button = '') {
                    let $popup = $('#js-popup-modal-id');

                    if ($popup.length) {
                        if (head.length) {
                            $popup.find('#js-popup-modal-head-id').text(head);
                        }
                        if (button.length) {
                            $popup.find('#js-popup-modal-button-id').text(button);
                        }
                        $popup.find('#js-popup-modal-text-id').text(text);
                        $popup.modal();
                    }
                },
                hide: function() {
                    let $popup = $('#js-popup-modal-id');

                    if ($popup.length) {
                        $popup.modal('hide');
                    }
                }
            },

            // Всплывающее окно.
            popup: {
                showed: false,
                show: function(sel, opts) {
                    if (typeof opts !== 'object') {
                        opts = {};
                    }
                    $('.popup').hide();
                    am.showed = false;

                    if (!opts.hasOwnProperty('titleTxt')) {
                        opts.titleTxt = '';
                    }
                    if (!opts.hasOwnProperty('closeTxt')) {
                        opts.closeTxt = '';
                    }

                    $(sel).find('.js-popup-title').html(opts.titleTxt);
                    $(sel).find('.js-popup-close').html(opts.closeTxt);

                    var h = $(window).scrollTop();
                    if (!device.mobile()) {
                        h += ($(window).height() - sel.height()) / 2;
                    }
                    sel.css('top', h + 'px');
                    sel.show();

                    setTimeout(function(){am.showed = true;}, 10);
                },
                hide: function() {
                    $('.popups .popup').hide();
                    am.showed = false;
                },
                message: function(title, text) {
                    var popup = $('.js-popup-message');

                    popup.find('.js-popup-text').html(text);
                    popup.find('.js-popup-button').click(function(e) {
                        e.preventDefault();
                        am.popup.hide();
                    });

                    am.popup.show(popup, {
                        titleTxt: title,
                    });
                },
                init: function() {
                    $(window).click(function(e) {
                        var el = $(e.target);

                        if (am.showed && !el.closest('.popup').length) {
                            am.popup.hide();
                        }
                    });

                    $('.popup').find('.js-popup-close').click(function(e) {
                        e.preventDefault();
                        am.popup.hide();
                    });

                    $(window).on('scroll resize', function() {
                        var pel = $('.popup:visible');
                        if (pel.length) {
                            var h = $(window).scrollTop();
                            if (!device.mobile()) {
                                h += ($(window).height() - pel.height()) / 2;
                            }
                            pel.css('top', h + 'px');
                        }
                    });
                }
            },

            // Работа с URL.
            url: {

                generateKeyForUrl: function(nel) {
                    let key = [];
                    nel.each(function() {
                        let v = $(this).val();
                        if (!v) {
                            return;
                        }

                        key.push(v);
                    });
                    return key.join('-');
                },

                autogeneratorUrlKey: function(sel, nel, rel) {
                    nel.bind('paste change blur keyup', null, function(e) {
                        setTimeout(function() {
                            let key;

                            key = am.url.generateKeyForUrl(nel);
                            key = am.url.generateUrlKey(key);
                            if (rel) {
                                key = key.replace(rel.val()+'-', '');
                            }
                            sel.prop('placeholder', key);
                        }, 100);
                    }).change(function() {
                        if (sel.val()) {
                            return;
                        }
                        let key;

                        key = am.url.generateKeyForUrl(nel);
                        key = am.url.generateUrlKey(key);
                        if (rel) {
                            key = key.replace(rel.val()+'-', '');
                        }
                        sel.val(key);
                    });

                    sel.bind('paste blur keyup', null, function(e) {
                        let key = $(this).val();
                        if (!key) {
                            key = $(this).attr('placeholder');
                        }
                        $(this).val(am.url.generateUrlKey(key));
                    }).change(function(e) {
                        if ($(this).val()) {
                            return;
                        }
                        let key = $(this).val();
                        if (!key) {
                            key = $(this).attr('placeholder');
                        }

                        key = am.url.generateUrlKey(key);
                        if (rel) {
                            key = key.replace(rel.val()+'-', '');
                        }
                        $(this).val(key);
                    });

                    var key = nel.val();
                    key = am.url.generateUrlKey(key);
                    if (rel) {
                        key = key.replace(rel.val()+'-', '');
                    }

                    sel.prop('placeholder', key);
                },

                generateUrlKey: function(t) {
                    var key = '';
                    if (t) key = t.toLowerCase();

                    return app.misc.url.translit(key);
                },

                // Транслитерация.
                translit: function(text) {
                    // Символ, на который будут заменяться все спецсимволы
                    var space = '-';

                    // Массив для транслитерации
                    var transl = {
                        'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
                        'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
                        'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
                        'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh','ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya',
                        ' ': space, '_': space, '`': space, '~': space, '!': space, '@': space,
                        '#': space, '$': space, '%': space, '^': space, '&': space, '*': space,
                        '(': '', ')': '','-': space, '\=': space, '+': space, '[': space,
                        ']': space, '\\': space, '|': space, '/': space,'.': space, ',': space,
                        '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
                        '?': space, '<': space, '>': space, '№':space, '«': '', '»': '',
                    }

                    var result = '';
                    var curent_sim = '';

                    for(i=0; i < text.length; i++) {
                        // Если символ найден в массиве то меняем его
                        if(transl[text[i]] != undefined) {
                            if(curent_sim != transl[text[i]] || curent_sim != space){
                                result += transl[text[i]];
                                curent_sim = transl[text[i]];
                            }
                        }
                        // Если нет, то оставляем так как есть
                        else {
                            result += text[i];
                            curent_sim = text[i];
                        }
                    }

                    return result;
                }
            },

            cUrl: {
                getParameter: function(param) {
                    var vars = {};
                    window.location.href.replace( location.hash, '' ).replace(
                        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
                        function( m, key, value ) { // callback
                            vars[key] = value !== undefined ? value : '';
                        }
                    );

                    if ( param ) {
                        return vars[param] ? vars[param] : null;
                    }
                    return vars;
                },
                setUrlParameter: function(key, value) {
                    key = escape(key); value = escape(value);
                    var kvp = document.location.search.substr(1).split('&');
                    var i=kvp.length; var x; while(i--)
                    {
                        x = kvp[i].split('=');

                        if (x[0]==key)
                        {
                            x[1] = value;
                            kvp[i] = x.join('=');
                            break;
                        }
                    }
                    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

                    //this will reload the page, it's likely better to store this until finished
                    document.location.search = kvp.join('&');
                },
                removeUrlParameter: function(k) {

                },

                setUrlParameter: function(param, value) {
                    var regexp = new RegExp("(\\?|\\&)" + param + "\\=([^\\&]*)(\\&|$)");
                    if (regexp.test(document.location.search))
                        return (document.location.search.toString().replace(regexp, function(a, b, c, d)
                        {
                            return (b + param + "=" + value + d);
                        }));
                    else
                        return document.location.search+ param + "=" + value;
                },
                parseAnchorQueryString: function(queryString) {
                    var params = {}, queries, temp, i, l;

                    queries = queryString.split("&");
                    for ( i = 0, l = queries.length; i < l; i++ ) {
                        temp = queries[i].split('=');
                        if (temp[0] == '') continue;

                        params[temp[0]] = temp[1];
                    }

                    return params;
                },
                getAnchorParams: function() {
                    var params = {};

                    try {
                        var hash = location.hash;
                        hash = hash.substring(1, hash.length);

                        params = app.misc.cUrl.parseAnchorQueryString(hash);
                    } catch(e){}

                    return params;
                },
                getAnchorParam: function(k) {
                    var v = null;

                    var params = app.misc.cUrl.getAnchorParams();
                    if (typeof params[k] == 'undefined') return null;

                    return params[k];
                },
                setAnchorParam: function(k, v) {
                    var params = app.misc.cUrl.getAnchorParams();
                    params[k] = v;

                    location.hash = '#'+$.param(params);
                },
                removeAnchorParam: function(k) {
                    var params = app.misc.cUrl.getAnchorParams();
                    delete params[k];

                    location.hash = '#'+$.param(params);
                }
            },

            slider: function(selector, options) {
                if (!selector) {
                    return;
                }
                if (typeof options !== 'object') {
                    return;
                }

                var slider = $(selector);
                if (!slider.size()) {
                    return;
                }

                slider.flexslider(options);

                slider.find('.js_prev').click(function(e){
                    e.preventDefault();
                    slider.flexslider('prev');
                });
                slider.find('.js_next').click(function(e){
                    e.preventDefault();
                    slider.flexslider('next');
                });

                slider.resize();
            },

            generateKey: function() {
                return (new Date).getTime();
            },

            insertParam: function(key, value) {
                key = encodeURI(key); value = encodeURI(value);

                var kvp = document.location.search.substr(1).split('&');

                var i = kvp.length; var x;
                while (i--) {
                    x = kvp[i].split('=');

                    if (x[0] == key) {
                        x[1] = value;
                        kvp[i] = x.join('=');
                        break;
                    }
                }

                if (i < 0) {
                    kvp[kvp.length] = [key,value].join('=');
                }
                document.location.search = kvp.join('&');
            },

            pluralize : function(number, one, two, many) {
                number = parseInt(number, 10);
                if (typeof number !== 'number' || isNaN(number)) {
                    number = 0;
                }
                number = Math.abs(number);
                var cases = [2, 0, 1, 1, 1, 2];
                var titles = $.isArray(one) ? one : [one, two, many];
                var rem = number % 100;
                return titles[(rem > 4 && rem < 20) ? 2 : cases[ Math.min(number % 10, 5)]];
            },

            naturalize: function(number) {
                number = parseInt(number, 10);
                if (typeof number !== 'number' || isNaN(number)) {
                    number = 0;
                }
                return Math.abs(number);
            },

            arrayToForm: function(data, opts) {
                if (typeof opts !== 'object') {
                    opts = {};
                }
                var appendTo = opts.appendTo || $('body');
                var attrs = {
                    id: opts.id || 'form_'+(new Date().getTime())+'_'+Math.floor(Math.random() * 10000000),
                    method: opts.method || 'post',
                    action: opts.action || '',
                    enctype: opts.enctype || 'multipart/form-data'
                };
                var fel = jQuery('<form></form>');
                fel.attr(attrs);
                fel.appendTo(appendTo);
                if (!opts.visible) {
                    fel.hide();
                }
                for (var di in data) {
                    var iel = jQuery('<input type="text" />');
                    iel.appendTo(fel);
                    iel.attr({name: di, value: data[di]});
                }
                return fel;
            },

            dateFormat: function(num, is_year){
                if (typeof num === 'number') num = num.toString();
                if (typeof num !== 'string') {
                    if (is_year) return '0000';
                    else return '00';
                }
                if (is_year) {
                    num = parseInt(num, 10);
                    if (isNaN(num)) num = 0;
                    if (num < 1900) num = 1900;
                    else if (num > (2100)) num = 2100;
                    num = num.toString();
                } else {
                    if (num.length < 1) num = '01';
                    else if (num.length == 1) num = '0'+num;
                    else num = num.substr(num.length - 2, 2);
                }
                return num;
            },

            runCmdExecuteQueryResourse: false,

            // XHR-запрос.
            runCmd: function(link, data, scb, ecb, opts) {
                var url = '/ajax/' + link;

                var params = {
                    type: 'POST',
                    dataType: 'json',
                    url: url,
                    data: data,
                    cache: false,
                    timeout: 40000,
                    success : function(res) {
                        if (!res || typeof res !== 'object') {
                            if (typeof ecb === 'function') {
                                ecb({"status": false,"text_status": "error", "code": 2,"message": "Connection error"});
                            }
                        } else if (res.status === false) {
                            if (typeof ecb === 'function') {
                                ecb(res);
                            }
                        } else {
                            if (typeof scb === 'function') {
                                scb(res);
                            }
                        }
                    },
                    error: function() {
                        if (typeof ecb === 'function') {
                            ecb({"status": false, "text_status": "error", "code": 2, "message": "Connection error"});
                        }
                    }
                };


                if (opts.hasOwnProperty('processData')) {
                    params.processData = opts.processData;
                    if (!params.processData) {
                        params.contentType = false;
                    }
                }

                am.runCmdExecuteQueryResourse = $.ajax(params);
            },


            // Обработка ajax-запроса.
            promiseCmd: function(link, data, opts) {
                var def = $.Deferred();

                if (typeof opts !== 'object') {
                    opts = {processData: true};
                }

                am.runCmd(link, data, function(response) {
                    def.resolve(response);
                }, function(error) {
                    if (error && typeof error === 'object' && error.action === 'login') {
                        app.user.callLogin();
                        def.reject(null);
                    } else if (error && typeof error === 'object' && error.action === 'reload') {
                        def.reject(error);
                        location.reload();
                    } else {
                        def.reject(error);
                    }
                }, opts);

                return def.promise();
            },

            // Логирование в консоли.
            log: function(message) {
                if (console && typeof console === 'object' && typeof console.log === 'function') {
                    console.log(message);
                }
            },

            // Очистка сгенерированных сообщений об ошибках.
            clearGenericFormErrors: function(form) {
                form.find('.js-field-error').html('');
                form.find('.js-glob-error').text('');
                form.find('.js-glob-warning').hide().text('');
                form.find('.has-error').removeClass('has-error');
            },

            // Очистка элементов формы.
            clearFormElements: function(form) {
                form.find('input').each(function() {
                    let type = $(this).prop('type');
                    if (type == 'button' || type == 'hidden' || type == 'submit') {
                        return;
                    }
                    $(this).val('');
                });
                form.find('textarea').val('');
            },

            /**
             * Обработка ошибок
             *
             * @param fel - форма
             * @param err - массив ошибок
             * @param opts - опции
             */
            showGenericFormErrors: function(fel, err, opts) {
                var type = err.msg == 'warning call' ? 'warning' : 'error';

                if (typeof opts !== 'object') {
                    opts = {};
                }

                var isScroll = false;
                if (opts.hasOwnProperty('scroll')) {
                    isScroll = true;
                }

                if (!err || typeof err !== 'object') {
                    return;
                }

                if (typeof err.data === 'object') {
                    $.each(err.data, function(name, txt) {
                        var msg = null;
                        if (typeof txt === 'string') {
                            msg = txt;
                        }

                        var errel = fel.find('.js-' + name + '-error');
                        var inpel = fel.find('input[name="' + name + '"], .js-field-' + name);

                        if (errel.length >= 1) {
                            if (msg) {
                                errel.html('<div class="text-danger js_field_error">' + msg + '</div>');
                            }

                            errel.parent().addClass('has-error');
                            if (isScroll) {
                                if ($.scrollTo) {
                                    if (errel.closest('.modal')) {
                                        $('.modal').scrollTo(errel.parent());
                                    } else {
                                        $.scrollTo(errel.parent());
                                    }
                                }
                            }
                        } else if (inpel.length >= 1) {
                            if (msg) {
                                errel.html('<div class="text-danger js_field_error">'+msg+'</div>');
                            }

                            inpel.addClass('jsField');
                            inpel.parent().addClass('has-error');
                            if (isScroll) {
                                if ($.scrollTo) {
                                    if (inpel.closest('.modal')) {
                                        $('.modal').scrollTo(inpel.parent());
                                    } else {
                                        $.scrollTo(inpel.parent());
                                    }
                                }
                            }
                        } else {
                            var pel = fel.find('.js-glob-' + type);
                            pel.hide();

                            if (msg) {
                                pel.text(msg).show();
                            }
                        }
                    });
                } else if (err.data) {
                    fel.find('.js-glob-' + type).text(err.data).show();
                } else if (err.msg_ru) {
                    fel.find('.js-glob-' + type).text(err.msg_ru).show();
                } else if (err.msg) {
                    fel.find('.js-glob-' + type).text(err.msg).show();
                } else {
                    fel.find('.js-glob-' + type).text('ошибка').show();
                }
            },

            hideGenericFormErrorsInGlob: function(fel) {
                fel.find('.js-glob-error').html('').hide();
                fel.find('.js-glob-warning').html('').hide();
            },

            showGenericFormErrorsInGlob: function(fel, err) {
                fel.find('.js-glob-error').html('').hide();
                fel.find('.js-glob-warning').html('').hide();

                if (!err || typeof err !== 'object') {
                    return;
                }
                var garr = [];

                if (typeof err.data === 'object') {

                    $.each(err.data, function(name, text) {
                        if (name === 'msg_en') {
                            return;
                        }
                        let message = '';
                        if (typeof text === 'string') {
                            message = text;
                        } else if (typeof text === 'object' && typeof text.ru === 'string') {
                            message = txt.ru;
                        }
                        garr.push(message);
                    });

                    if (err.data && err.data.ru) {
                        garr.push(err.data.ru);
                    } else if (err.data && err.data.en) {
                        garr.push(err.data.en);
                    }
                } else if (err.data) {
                    garr.push(err.data);
                } else if (err.msg_ru) {
                    garr.push(err.msg_ru);
                } else if (err.msg) {
                    garr.push(err.msg);
                } else {
                    garr.push('Ошибка');
                }
                fel.find('.js-glob-error').html(garr.join('<br />\n')).show();
            },

            // Показ списка сообщений.
            showMessageList: function(element, list) {
                if (!$.isArray(list)) {
                    return;
                }
                var messages = $.grep(list, function(message) {
                    return (typeof v === 'string');
                });
                if (messages.length > 0) {
                    element.css('color', 'green').html(messages.join('<br/>')).show();
                }
            }
        },

        get: function(moduleName) {
            var md = app.moduleLoadDefs[moduleName];
            if (typeof md === 'object' && typeof md.d === 'object' && typeof md.p === 'object') {
                return md.p;
            }
            var def = $.Deferred();
            var pr = def.promise();
            md = {d: def, p: pr};
            app.moduleLoadDefs[moduleName] = md;
            if (typeof app[moduleName] === 'object') {
                def.resolve(app[moduleName]);
            }
            return pr;
        },

        moduleLoadDefs: {},
        onModuleLoaded: function() {
            for (var mi in app.moduleLoadDefs) {
                var md = app.moduleLoadDefs[mi];
                if (typeof app[mi] === 'object' && typeof md === 'object' && typeof md.d === 'object') {
                    md.d.resolve(app[mi]);
                }
            }
        },

        get: function(mp) {
            var md = app._getModuleDef(mp);
            if (md.d && typeof md.d === 'object' && typeof md.d.promise === 'function') return md.d.promise();
            md.d = $.Deferred();
            var m = app._getModuleByPath(mp);
            if (m && typeof m === 'object') md.d.resolve(m);
            return md.d.promise();
        },

        _getModuleByPath: function(mp) {
            if (typeof mp === 'string') mp = [mp];
            return app._getPropByPath(mp, app);
        },

        _getPropByPath: function(p, o){
            if (!$.isArray(p)) throw new TypeError('invalid path');
            if (p.length < 1) throw new TypeError('empty path');
            var n = p[0];
            if (!n || typeof n !== 'string') throw new TypeError('invalid path part');
            if (!o || typeof o !== 'object') return null;
            if (!o[n] || typeof o[n] !== 'object') return null;
            if (p.length === 1) return o[n];
            return app._getPropByPath(p.slice(1), o[n]);
        },

        _moduleLoadDefs: {},
        _getModuleDef: function(mp) {
            if (typeof mp === 'string') mp = [mp];
            var res = app._prepareModuleDefStruct(mp, app._moduleLoadDefs);
            app._moduleLoadDefs = res[1];
            return res[0];
        },

        _prepareModuleDefStruct: function(p, s) {
            if (!$.isArray(p)) throw new TypeError('invalid module path');
            if (p.length < 1) throw new TypeError('empty module path');
            var mn = p[0];
            if (!mn || typeof mn !== 'string') throw new TypeError('invalid module path part');
            if (!s || typeof s !== 'object') throw new TypeError('invalid module struct');
            if (!s[mn] || typeof s[mn] !== 'object') s[mn] = {};
            if (p.length === 1) return [s[mn], s];
            if (!s[mn].l || typeof s[mn].l !== 'object') s[mn].l = {};
            var res = app._prepareModuleDefStruct(p.slice(1), s[mn].l);
            s[mn].l = res[1];
            return [res[0], s];
        },

        _resolveDefTree: function(p, t) {
            if (!$.isArray(p)) throw new TypeError('invalid path');
            if (!t || typeof t !== 'object') return;
            for (var mn in t) {
                var md = t[mn];
                var mp = p.concat(mn);
                if (md.d && typeof md.d === 'object' && typeof md.d.resolve === 'function') {
                    var m = app._getModuleByPath(mp);
                    if (m && typeof m === 'object') {
                        md.d.resolve(m);
                    }
                }
                if (md.l && typeof md.l === 'object') {
                    app._resolveDefTree(mp, md.l);
                }
            }
        },

        _onModuleLoaded: function() {
            app._resolveDefTree([], app._moduleLoadDefs);
        },

        init: function() {
            if (!$.isArray(appasyncadd)) {
                return;
            }

            for (let i = 0; i < appasyncadd.length; i++) {
                if (typeof appasyncadd[i] === 'function') {
                    appasyncadd[i](app);
                    app._onModuleLoaded();
                }
            }
            appasyncadd = {
                push: function(loader) {
                    if (typeof loader === 'function') {
                        loader(app);
                        app._onModuleLoaded();
                    }
                }
            };
        }
    };

    var am = {};
    var loader = function() {
        $.app = app;
        am = app.misc;
        jQuery(function() {
            app.init();
            am.popup.init();
        });
    };

    var ali = setInterval(function() {
        if (typeof jQuery !== 'function') {
            return;
        }
        clearInterval(ali);
        setTimeout(loader, 0);
    }, 50);
})();


// Расширение jQuery.
(function($) {

    // Преобразование первого символа строки в верхний регистр.
    $.ucfirst = function(string) {
        let text  = string;
        let parts = text.split(' ');
        let words = [];

        for (let i = 0; i < parts.length; i++) {
            var part  = parts[i];
            var first = part[0].toUpperCase();
            var rest  = part.substring(1, part.length);
            var word  = first + rest;
            words.push(word);
        }
        return words.join(' ');
    };
})(jQuery);