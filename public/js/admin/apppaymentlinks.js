var appasyncadd = appasyncadd || [];

(function() {
    var app = {}, am = {}, self = {};

    // Загрузка модуля.
    var loader = function(application) {
        app  = application;
        am   = app.misc;
        self = module;
        self.init();
    };

    setTimeout( function() {
        appasyncadd.push(loader);
    }, 0);

    // Глобальные объекты.
    var busy = false;

    // Модуль страницы.
    var module = {
        /**
         * Очистка форм
         */
        clearForms: function(form) {
            form.find('input').each(function () {
                let type = $(this).prop('type');
                if (type == 'button' || type == 'hidden' || type == 'submit') {
                    return;
                }
                $(this).val('');
            });
            form.find('textarea').val('');
        },
        /**
         * Очистка ошибок
         */



        /**
         * Поп-ап для создания/редактирования ссылок
         */
        showLinkPopup: function(linkId, popup) {

            if (busy) return;
            busy = true;

            am.clearGenericFormErrors(popup);
            // Отображение поп-апа
            popup.modal();
            // Заголовок окна
            var title = popup.find('.jsPopupTitle');

            // Ajax-запрос
            am.promiseCmd(
                'default/getlinkdata',
                {'id': linkId}
            ).always(function(response) {
                busy = false;
            }).done(function(response) {
                title.html('Создать ссылку');
                if (response.data.id) {
                    title.html('Редактировать ссылку #'+response.data.id);
                }
                popup.find('.jsId').val(response.data.id);
                popup.find('.jsAmount').val(response.data.amount);
                popup.find('.jsDesc').val(response.data.desc);
                popup.find('.jsСurrency').val(response.data.currency);
                popup.find('.jsBot').val(response.data.bot);

                popup.find('.jsQuantity').val(response.data.quantity);
                popup.find('.jsVat').val(response.data.vat);
                popup.find('.jsMethod').val(response.data.method);
                popup.find('.jsObject').val(response.data.object);
                popup.find('.jsMeasurementUnit').val(response.data.measurementUnit);

            }).fail(function(err) {
                am.showGenericFormErrors(popup, err, {scroll: true});
            });

        },
        /**
         * Сохраняет ссылку в базе
         */
        saveLink: function(popup, form) {
            if (busy) return;
            busy = true;

            am.clearGenericFormErrors(form);

            var formData = form.serializeArray();

            // Допушим эти два поля к дате, т.к. они disabled
            var currency = form.find('.jsCurrency').val();
            formData.push({'name':'form[currency]','value':currency});
            var measurementUnit = form.find('.jsMeasurementUnit').val();
            formData.push({'name':'form[measurementUnit]','value':measurementUnit});

            am.promiseCmd(
                'default/setlinkdata',
                formData,
            ).always(function() {
                busy = false;
            }).done(function(res) {
                location.reload();
            }).fail(function(err) {
                am.showGenericFormErrors(form, err, {scroll: true});
            });
        },
        /**
         * Удаляет ссылку
         */
        deleteLink: function(id, action) {
            if (busy) return;
            if (!confirm('Вы уверены, что хотите '+action+'?')) return;
            busy = true;

            am.promiseCmd(
                'default/deletelink',
                { 'id': id }
            ).always(function() {
                busy = false;
            }).done(function(res) {
                location.reload();
            }).fail(function(err) {
            });
        },
        /**
         * Инициализация модуля.
         */
        init: function() {
            var popup = $('.jsAdminLinkForm');
            var form = popup.find('form');
            $('.jsShowAdminLinkCreatePopup').click(function (e) {
                e.preventDefault();
                self.showLinkPopup(null, popup);
            });
            $('.jsShowAdminLinkEditPopup').click(function (e) {
                e.preventDefault();
                self.showLinkPopup($(this).data('id'), popup);
            });
            $('.jsAdminLinkDelete').click(function (e) {
                e.preventDefault();
                self.deleteLink($(this).data('id'), $(this).data('action'));
            });
            form.submit(function (e) {
                e.preventDefault();
                self.saveLink(popup, form);
            });
        }
    };
})();
